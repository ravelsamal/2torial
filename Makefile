build-up:
	docker compose up --build

up:
	docker compose up

down:
	docker compose down

start:
	docker compose start

package main

import (
	"log"
	"net/http"

	"gitlab.com/ravelsamal/2torial/front-end/internal/models"
	"gitlab.com/ravelsamal/2torial/front-end/internal/models/db"
)

func main() {
	pool, err := db.OpenDB("postgresql://auth_service:auth_service@auth-db:5432/auth_service")
	if err != nil {
		log.Fatalln(err)
	}
	defer pool.Close()

	model := models.NewVideoModel(pool)

	h := &Handler{
		m: model,
	}

	handler := routes(h)

	s := http.Server{
		Addr:    ":80",
		Handler: handler,
	}

	s.ListenAndServe()
}

package main

import (
	"net/http"

	"github.com/justinas/nosurf"
)

type templateData struct {
	CSRFToken string
}

func newTemplateData(r *http.Request) *templateData {
	return &templateData{
		CSRFToken: nosurf.Token(r),
	}
}

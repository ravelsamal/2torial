package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Registers all the routes then returns an http.Handler
func routes(h *Handler) http.Handler {

	r := httprouter.New()
	r.HandlerFunc(http.MethodGet, "/", h.home)
	r.Handler(http.MethodGet, "/login", noSurf(http.HandlerFunc(login)))
	r.Handler(http.MethodGet, "/register", noSurf(http.HandlerFunc(register)))
	r.HandlerFunc(http.MethodGet, "/tutorial/:id", h.tutorial)
	r.Handler(http.MethodGet, "/dashboard/upload", noSurf(http.HandlerFunc(authAdmin(upload))))
	r.HandlerFunc(http.MethodGet, "/dashboard", authAdmin(dashboard))
	r.ServeFiles("/static/*filepath", http.Dir("./ui/static"))

	return r
}

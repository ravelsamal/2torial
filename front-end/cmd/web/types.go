package main

import "time"

type User struct {
	FullName  string `json:"full_name"`
	Username  string `json:"username"`
	IsAdmin   bool   `json:"is_admin"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	CreatedAt time.Time
}

type username string

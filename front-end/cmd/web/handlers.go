package main

import (
	"html/template"
	"log"
	"net/http"
	"sync"

	"github.com/julienschmidt/httprouter"
	"github.com/justinas/nosurf"
	"gitlab.com/ravelsamal/2torial/front-end/internal/models"
)

type Handler struct {
	m *models.VideoModel
}

func (h *Handler) home(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./ui/html/base.tmpl.html",
		"./ui/html/partials/header.tmpl.html",
		"./ui/html/partials/footer.tmpl.html",
		"./ui/html/pages/home.tmpl.html",
	}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		log.Println(err)
	}

	videos, err := h.m.GetVideos()
	if err != nil || len(videos) == 0 {
		err = ts.ExecuteTemplate(w, "base", nil)
		if err != nil {
			log.Println(err)
		}
		return
	}

	var wg sync.WaitGroup
	for i, v := range videos {
		wg.Add(1)
		go func(i int, v *models.Video) {
			if len(v.Description) > 250 {
				videos[i].Description = v.Description[:250]
			}
			wg.Done()
		}(i, v)
	}
	wg.Wait()

	data := struct {
		Tutorials []*models.Video
	}{
		Tutorials: videos,
	}
	err = ts.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Println(err)
	}
}

func (h *Handler) tutorial(w http.ResponseWriter, r *http.Request) {
	id := httprouter.ParamsFromContext(r.Context()).ByName("id")
	if id == "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	video, err := h.m.GetVideo(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("404 page not found"))
		return
	}

	files := []string{
		"./ui/html/base.tmpl.html",
		"./ui/html/partials/header.tmpl.html",
		"./ui/html/partials/footer.tmpl.html",
		"./ui/html/pages/tutorial.tmpl.html",
	}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		log.Println(err)
	}

	err = ts.ExecuteTemplate(w, "base", video)
	if err != nil {
		log.Println(err)
	}
}

func register(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./ui/html/base.tmpl.html",
		"./ui/html/partials/header.tmpl.html",
		"./ui/html/partials/footer.tmpl.html",
		"./ui/html/pages/register.tmpl.html",
	}

	ts, err := template.ParseFiles(files...)
	if err != nil {
		log.Println(err)
	}

	data := newTemplateData(r)
	err = ts.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Println(err)
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./ui/html/base.tmpl.html",
		"./ui/html/pages/login.tmpl.html",
		"./ui/html/partials/header.tmpl.html",
		"./ui/html/partials/footer.tmpl.html",
	}
	ts, err := template.ParseFiles(files...)
	if err != nil {
		log.Println(err)
		return
	}

	data := newTemplateData(r)
	err = ts.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Println(err)
		return
	}
}

func upload(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./ui/html/dashboard/base.tmpl.html",
		"./ui/html/dashboard/partials/header.tmpl.html",
		"./ui/html/dashboard/pages/upload.tmpl.html",
	}

	t, err := template.ParseFiles(files...)
	if err != nil {
		log.Println(err)
		return
	}

	data := struct {
		IsAdmin   bool
		CSRFToken string
	}{
		IsAdmin:   true,
		CSRFToken: nosurf.Token(r),
	}

	err = t.ExecuteTemplate(w, "base", data)
	if err != nil {
		log.Println(err)
		return
	}
}

func dashboard(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./ui/html/dashboard/base.tmpl.html",
		"./ui/html/dashboard/partials/header.tmpl.html",
		"./ui/html/dashboard/pages/home.tmpl.html",
	}

	t, err := template.ParseFiles(files...)
	if err != nil {
		log.Println(err)
		return
	}

	err = t.ExecuteTemplate(w, "base", nil)
	if err != nil {
		log.Println(err)
		return
	}
}

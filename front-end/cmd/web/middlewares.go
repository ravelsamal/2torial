package main

import (
	"context"
	"log"
	"net/http"

	"github.com/justinas/nosurf"
	"gitlab.com/ravelsamal/2torial/front-end/internal/helpers"
)

func authAdmin(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("access_token")
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			log.Println(err)
			return
		}

		ts, err := helpers.NewTokenService("/run/secrets/jwt-secret-key")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}

		claims, err := ts.VerifyToken(cookie.Value)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			log.Println(err)
			return
		}

		if !claims.Admin {
			w.WriteHeader(http.StatusUnauthorized)
			return

		}

		ctx := context.WithValue(r.Context(), "username", claims.Username)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})
}

func noSurf(next http.Handler) http.Handler {
	handler := nosurf.New(next)
	handler.SetBaseCookie(http.Cookie{
		HttpOnly: true,
		Path:     "/",
	})
	return handler
}

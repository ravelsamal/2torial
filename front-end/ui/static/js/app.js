const passwordVisibilityBtn = document.getElementById("password-visibility");
const passwordField = document.getElementById("password");
const registerForm = document.getElementById("register-form");
const loginForm = document.getElementById("login-form");
const uploadForm = document.getElementById("upload-form");

const setIconToVisible = (node) => {
  node.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="M480-320q75 0 127.5-52.5T660-500q0-75-52.5-127.5T480-680q-75 0-127.5 52.5T300-500q0 75 52.5 127.5T480-320Zm0-72q-45 0-76.5-31.5T372-500q0-45 31.5-76.5T480-608q45 0 76.5 31.5T588-500q0 45-31.5 76.5T480-392Zm0 192q-146 0-266-81.5T40-500q54-137 174-218.5T480-800q146 0 266 81.5T920-500q-54 137-174 218.5T480-200Zm0-300Zm0 220q113 0 207.5-59.5T832-500q-50-101-144.5-160.5T480-720q-113 0-207.5 59.5T128-500q50 101 144.5 160.5T480-280Z" /></svg>';
}

const setIconToInvisible = (node) => {
  node.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 -960 960 960" width="24"><path d="m644-428-58-58q9-47-27-88t-93-32l-58-58q17-8 34.5-12t37.5-4q75 0 127.5 52.5T660-500q0 20-4 37.5T644-428Zm128 126-58-56q38-29 67.5-63.5T832-500q-50-101-143.5-160.5T480-720q-29 0-57 4t-55 12l-62-62q41-17 84-25.5t90-8.5q151 0 269 83.5T920-500q-23 59-60.5 109.5T772-302Zm20 246L624-222q-35 11-70.5 16.5T480-200q-151 0-269-83.5T40-500q21-53 53-98.5t73-81.5L56-792l56-56 736 736-56 56ZM222-624q-29 26-53 57t-41 67q50 101 143.5 160.5T480-280q20 0 39-2.5t39-5.5l-36-38q-11 3-21 4.5t-21 1.5q-75 0-127.5-52.5T300-500q0-11 1.5-21t4.5-21l-84-82Zm319 93Zm-151 75Z"/></svg>';
}

passwordVisibilityBtn?.addEventListener("click", () => {
  const fieldType = passwordField.getAttribute("type");
  if(fieldType === "password") {
    passwordField.setAttribute("type", "text");
    setIconToVisible(passwordVisibilityBtn)
    return;
  }

  passwordField.setAttribute("type", "password");
  setIconToInvisible(passwordVisibilityBtn);
});

const validateFullName = fullName => {
  if(!fullName) {
    throw new Error("Full name is required.")
  }

  if(fullName.length < 2) {
    throw new Error("Full name must be at least 2 characters.")
  }

  if(fullName.length > 120) {
    throw new Error("Full name must be less than 120 characters.")
  }

  return true
}

const validateUsername = username => {
  if(!username) {
    throw new Error("Username is required.")
  }

  if(username.length < 5) {
    throw new Error("Username must be at least 5 characters.")
  }

  if(username.length > 120) {
    throw new Error("Username must be less than 30 characters.")
  }

  return true
}

const validateEmail = email => {
  if(!email) {
    throw new Error("Email is required.")
  }

  return true
}

const validatePassword = (password, passwordConfirm) => {
  if(!password) {
    throw new Error("Password is required.")
  }

  if(password.length < 7) {
    throw new Error("Password must be at least 7 characters.")
  }

  if(password.length > 40) {
    throw new Error("Password must be less than 40 characters.")
  }

  if(passwordConfirm !== null && password !== passwordConfirm) {
    throw new Error("Password does not match");
  }

  return true
}

registerForm?.addEventListener("submit", async e => {
  e.preventDefault();
  try {
    const formData = new FormData(registerForm);
    validateFullName(formData.get("full_name"));
    validateUsername(formData.get("username"));
    validateEmail(formData.get("email"));
    validatePassword(formData.get("password"), formData.get("re-password"));
    const errorMessage = document.getElementById("error-message");
    errorMessage ? errorMessage.innerHTML = "" : "";
    const userData = {
      full_name: formData.get("full_name"),
      username: formData.get("username"),
      email: formData.get("email"),
      password: formData.get("password")
    }

    try {
      const response = await fetch("/api/register", {
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          "Content-Type": "application/json",
          "X-Csrf-Token": formData.get("csrf_token")
        }
      });

      if(!response.ok) {
        const body = (await response.json()).message;
        if(typeof body === "string") {
          errorMessage ? errorMessage.innerHTML =  `*${body}`: "";
          return
        }

        for (const key in body) {
          const message = body[key];
          errorMessage ? errorMessage.innerHTML = `*${message}` : "";
        }

        return
      }

      window.location.replace("/");
    } catch (error) {}
  } catch(error) {
    const errorMessage = document.getElementById("error-message");
    errorMessage ? errorMessage.innerHTML = `*${error.message}` : "";
  }
})

loginForm?.addEventListener("submit", async e => {
  e.preventDefault();
  try {
    const formData = new FormData(loginForm);
    validateUsername(formData.get("username"));
    validatePassword(formData.get("password"), null);
    const errorMessage = document.getElementById("error-message");
    errorMessage ? errorMessage.innerHTML = "" : "";
    const userData = {
      username: formData.get("username"),
      password: formData.get("password")
    }

    try {
      const response = await fetch("/api/login", {
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          "X-Csrf-Token": formData.get("csrf_token"),
          "Content-Type": "application/json",
        },
        credentials: "include"
      });

      if(!response.ok) {
        const body = (await response.json()).message;
        if(typeof body === "string") {
          errorMessage ? errorMessage.innerHTML =  `*${body}`: "";
          return
        }

        for (const key in body) {
          const message = body[key];
          errorMessage ? errorMessage.innerHTML = `*${message}` : "";
        }

        return
      }

      if(document.cookie?.split("=")[1] == "true") {
        document.cookie = "";
        window.location.replace("/dashboard");
        return
      }
      console.log(document.cookie?.split("="))

      window.location.replace("/");
    } catch (error) {}
  } catch(error) {
    const errorMessage = document.getElementById("error-message");
    errorMessage ? errorMessage.innerHTML = `*${error.message}` : "";
  }
})

uploadForm?.addEventListener("submit", async e => {
  e.preventDefault();
  const formData = new FormData(uploadForm);

  const response = await fetch("/stream/videos/upload", {
    method: "POST",
    credentials: "include",
    body: formData,
  })

  if(!response.ok) {
    const body = (await response.json()).message;
    if(typeof body === "string") {
      errorMessage ? errorMessage.innerHTML =  `*${body}`: "";
      return
    }

    for (const key in body) {
      const message = body[key];
      errorMessage ? errorMessage.innerHTML = `*${message}` : "";
    }

    return
  }
  window.location.replace("/dashboard")
})

tailwind.config = {}

package helpers

import (
	"errors"
	"os"

	"github.com/golang-jwt/jwt/v5"
)

type TokenService struct {
	key []byte
}

type UserClaims struct {
	Username string
	Admin    bool
	jwt.RegisteredClaims
}

func NewTokenService(path string) (*TokenService, error) {
	content, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	key := content[:len(content)-1]

	return &TokenService{
		key: key,
	}, nil
}

func (t TokenService) VerifyToken(tokenStr string) (*UserClaims, error) {
	token, err := jwt.ParseWithClaims(tokenStr, &UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return t.key, nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*UserClaims)
	if !ok && !token.Valid {
		return nil, errors.New("Invalid token")
	}

	return claims, nil
}

package models

import (
	"context"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Video struct {
	ID          string
	Title       string
	Description string
	URL         string
	Author      string
	Thumbnail   string
}

type VideoModel struct {
	db *pgxpool.Pool
}

func NewVideoModel(db *pgxpool.Pool) *VideoModel {
	return &VideoModel{
		db: db,
	}
}

func (vm VideoModel) GetVideos() ([]*Video, error) {
	stmt := `SELECT id, title, author, url, thumbnail, description
		FROM video
		WHERE published = 't';`
	rows, err := vm.db.Query(context.Background(), stmt)
	if err != nil {
		return nil, err
	}

	var videos []*Video
	for rows.Next() {
		var video Video
		rows.Scan(&video.ID, &video.Title, &video.Author, &video.URL, &video.Thumbnail, &video.Description)
		videos = append(videos, &video)
	}

	return videos, nil
}

func (vm VideoModel) GetVideo(id string) (*Video, error) {
	stmt := "SELECT id, title, author, url, thumbnail, description FROM video WHERE id = $1;"
	row := vm.db.QueryRow(context.Background(), stmt, id)
	var video Video
	err := row.Scan(&video.ID, &video.Title, &video.Author, &video.URL, &video.Thumbnail, &video.Description)
	if err != nil {
		return nil, err
	}

	return &video, nil
}

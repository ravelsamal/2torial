module gitlab.com/ravelsamal/2torial/front-end

go 1.21.0

require (
	github.com/golang-jwt/jwt/v5 v5.0.0
	github.com/jackc/pgx/v5 v5.4.3
	github.com/julienschmidt/httprouter v1.3.0
	github.com/justinas/nosurf v1.1.1
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)

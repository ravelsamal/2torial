package video

import (
	"context"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Video struct {
	ID          string
	Title       string
	Description string
	URL         string
	Author      string
	Thumbnail   string
	Published   bool
}

type VideoModel struct {
	db *pgxpool.Pool
}

func NewVideoModel(db *pgxpool.Pool) *VideoModel {
	return &VideoModel{
		db: db,
	}
}

func (vm *VideoModel) Publish(id string) error {
	stmt := `UPDATE video SET published = $1
	WHERE id = $2`
	_, err := vm.db.Exec(context.Background(), stmt, true, id)
	if err != nil {
		return err
	}

	return nil
}

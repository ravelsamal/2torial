package queue

import (
	"context"
	"errors"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

type Queue interface {
	Receive() (string, error)
}

type SQS struct {
	client *sqs.Client
	receit *string
}

func NewSQS() (*SQS, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		return nil, err
	}
	return &SQS{
		client: sqs.NewFromConfig(cfg),
	}, nil
}

func (pub *SQS) Receive() (string, error) {
	queueURL, err := pub.client.GetQueueUrl(context.Background(), &sqs.GetQueueUrlInput{
		QueueName: aws.String("videosToBeProcessed"),
	})
	if err != nil {
		return "", err
	}
	msgOutput, err := pub.client.ReceiveMessage(context.Background(), &sqs.ReceiveMessageInput{
		QueueUrl:            queueURL.QueueUrl,
		MaxNumberOfMessages: *aws.Int32(1),
	})
	if err != nil {
		return "", err
	}
	if len(msgOutput.Messages) < 1 {
		return "", errors.New("Empty queue: no messages")
	}
	pub.receit = msgOutput.Messages[0].ReceiptHandle
	return *(msgOutput.Messages[0].Body), nil
}

func (pub *SQS) Delete() error {
	queueURL, err := pub.client.GetQueueUrl(context.Background(), &sqs.GetQueueUrlInput{
		QueueName: aws.String("videosToBeProcessed"),
	})
	if err != nil {
		return err
	}
	_, err = pub.client.DeleteMessage(context.Background(), &sqs.DeleteMessageInput{
		QueueUrl:      queueURL.QueueUrl,
		ReceiptHandle: pub.receit,
	})
	if err != nil {
		return err
	}
	return nil
}

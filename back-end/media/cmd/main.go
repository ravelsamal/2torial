package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/ravelsamal/2torial/back-end/media/internal/storage"
	"gitlab.com/ravelsamal/2torial/back-end/media/internal/video"
	"gitlab.com/ravelsamal/2torial/back-end/media/queue"
)

func main() {
	db, err := pgxpool.New(context.Background(), "postgresql://auth_service:auth_service@auth-db:5432/auth_service")
	if err != nil {
		log.Printf("Cannot connect to the database: %v\n", err)
	}

	queue, err := queue.NewSQS()
	if err != nil {
		log.Println(err)
	}

	for {
		msg, err := queue.Receive()
		if err != nil {
			log.Println(err)
			time.Sleep(time.Second * 20)
			continue
		}

		var v video.Video
		err = json.Unmarshal([]byte(msg), &v)
		if err != nil {
			log.Println(err)
		}

		store, err := storage.NewS3()
		if err != nil {
			log.Println(err)
		}

		vid, err := store.Get(v.ID)
		if err != nil {
			log.Println(err)
		}
		f, err := os.Create(fmt.Sprintf("%s.mp4", v.ID))
		if err != nil {
			log.Println(err)
		}
		_, err = io.Copy(f, vid)
		if err != nil {
			log.Println(err)
		}
		defer f.Close()
		os.MkdirAll(fmt.Sprintf("videos/%s/", v.ID), os.ModeAppend)
		cmd := exec.Command("ffmpeg", "-i", fmt.Sprintf("./%s.mp4", v.ID), "-b:v", "1M", "-g", "60", "-hls_time", "2", "-hls_list_size", "0", "-hls_segment_size", "500000", fmt.Sprintf("./videos/%s/video.m3u8", v.ID))
		err = cmd.Run()
		if err != nil {
			log.Println(err)
		}

		entries, err := os.ReadDir(fmt.Sprintf("videos/%s/", v.ID))
		if err != nil {
			log.Println(err)
		}

		for _, file := range entries {
			f, err := os.Open(fmt.Sprintf("videos/%s/%s", v.ID, file.Name()))
			if err != nil {
				log.Println(err)
				continue
			}
			store.Save(f, fmt.Sprintf("videos/%s/m3u8/%s", v.ID, file.Name()))
			f.Close()
		}

		err = queue.Delete()
		if err != nil {
			log.Println(err)
		}

		videoModel := video.NewVideoModel(db)
		videoModel.Publish(v.ID)
	}
}

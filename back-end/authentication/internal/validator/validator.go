package validator

import (
	"fmt"
	"unicode"

	"github.com/go-playground/validator/v10"
)

type Validator struct {
	FieldErrors map[string]string
}

type UserValidator struct {
	Validator
}

func NewUserValidator() *UserValidator {
	validator := &UserValidator{}
	validator.FieldErrors = make(map[string]string)

	return validator
}

// a helper function used by ValidateFullName function
func isName(field validator.FieldLevel) bool {
	for _, v := range field.Field().String() {
		if !(unicode.IsLetter(v) || unicode.IsSpace(v)) {
			return false
		}
	}

	return true
}

// only allows letters and spaces. From 2 to 120 characters
func (v *UserValidator) ValidateFullName(fullName string) bool {
	validate := validator.New(validator.WithRequiredStructEnabled())
	validate.RegisterValidation("name", isName)

	errs := validate.Var(fullName, "required,name,min=2,max=120")
	if errs != nil {
		for _, err := range errs.(validator.ValidationErrors) {
			if err.Tag() == "required" {
				fmt.Println("Full name is required")
			}

			if err.Tag() == "name" {
				v.FieldErrors["fullname"] = "Full name must consist of letters characters"
			}

			if err.Tag() == "min" {
				v.FieldErrors["fullname"] = fmt.Sprintf("Full name must be greater than %s characters", err.Param())
			}

			if err.Tag() == "max" {
				v.FieldErrors["fullname"] = fmt.Sprintf("Full name must not exceed %s characters", err.Param())
			}
			return false
		}
	}

	return true
}

// only allows letters alpha characters. From 5 to 30 characters
func (v *UserValidator) ValidateUsername(username string) bool {
	validate := validator.New(validator.WithRequiredStructEnabled())

	errs := validate.Var(username, "required,alpha,min=5,max=30")
	if errs != nil {
		for _, err := range errs.(validator.ValidationErrors) {
			if err.Tag() == "required" {
				v.FieldErrors["username"] = "Username is required"
			}

			if err.Tag() == "alpha" {
				v.FieldErrors["username"] = "Username must consist of alpha characters"
			}

			if err.Tag() == "min" {
				v.FieldErrors["username"] = fmt.Sprintf("Username must be greater than %s characters", err.Param())
			}

			if err.Tag() == "max" {
				v.FieldErrors["username"] = fmt.Sprintf("Username must not exceed %s characters", err.Param())
			}
		}
		return false
	}

	return true
}

// only allows email format
func (v *UserValidator) ValidateEmail(email string) bool {
	validate := validator.New(validator.WithRequiredStructEnabled())

	errs := validate.Var(email, "required,email")
	if errs != nil {
		for _, err := range errs.(validator.ValidationErrors) {
			if err.Tag() == "required" {
				v.FieldErrors["email"] = "Email is required"
			}

			if err.Tag() == "email" {
				v.FieldErrors["email"] = "Invalid email format"
			}
		}
		return false
	}

	return true
}

// seven to 120 characters are allowed
func (v *UserValidator) ValidatePassword(password string) bool {
	validate := validator.New(validator.WithRequiredStructEnabled())

	errs := validate.Var(password, "required,min=7,max=72")
	if errs != nil {
		for _, err := range errs.(validator.ValidationErrors) {
			if err.Tag() == "required" {
				fmt.Println("Password is required")
			}

			if err.Tag() == "min" {
				fmt.Printf("Password must be greater than %s characters", err.Param())
			}

			if err.Tag() == "max" {
				fmt.Printf("Password must not exceed %s characters", err.Param())
			}
		}
		return false
	}

	return true
}

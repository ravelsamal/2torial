package models

import (
	"context"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
)

type User struct {
	FullName  string `json:"full_name"`
	Username  string `json:"username"`
	IsAdmin   bool   `json:"is_admin"`
	Email     string `json:"email"`
	Password  string `json:"password"`
	CreatedAt time.Time
}

type UserModel struct {
	db *pgxpool.Pool
}

func NewUserModel(db *pgxpool.Pool) *UserModel {
	return &UserModel{
		db: db,
	}
}

func (m UserModel) Save(u User) error {
	u.CreatedAt = time.Now()
	stmt := "INSERT INTO account (full_name, username, email, password, created) VALUES ($1, $2, $3, $4, $5);"
	_, err := m.db.Exec(context.Background(), stmt, u.FullName, u.Username, u.Email, u.Password, u.CreatedAt)
	if err != nil {
		return err
	}

	return nil
}

func (m UserModel) FindUserByUsername(username string) (*User, error) {
	stmt := "SELECT full_name, username, email, password, admin FROM account WHERE username = $1;"
	row := m.db.QueryRow(context.Background(), stmt, username)

	var user User
	err := row.Scan(&user.FullName, &user.Username, &user.Email, &user.Password, &user.IsAdmin)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (m UserModel) FindUserByEmail(email string) (*User, error) {
	stmt := "SELECT full_name, username, email, password FROM account WHERE email = $1;"
	row := m.db.QueryRow(context.Background(), stmt, email)

	var user User
	err := row.Scan(&user.FullName, &user.Username, &user.Email, &user.Password)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

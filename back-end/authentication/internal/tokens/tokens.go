package tokens

import (
	"os"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/models"
)

type TokenService struct {
	key []byte
}

type UserClaims struct {
	Username string
	Admin    bool
	jwt.RegisteredClaims
}

// takes the path to secret key file returns a *PathError if there is no file
// found.
func NewTokenService(path string) (*TokenService, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	data = data[:len(data)-1]

	return &TokenService{
		key: data,
	}, nil
}

func (t *TokenService) GenerateToken(u *models.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": u.Username,
		"admin":    u.IsAdmin,
		"exp":      jwt.NewNumericDate(time.Now().Add(time.Hour * 24)),
	})

	tokenString, err := token.SignedString(t.key)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

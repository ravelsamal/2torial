package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/models"
	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/tokens"
	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/validator"
)

func (a *App) register(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		serverError(w)
		fmt.Println(err)
		return
	}

	var u models.User
	err = json.Unmarshal(body, &u)
	if err != nil {
		clientError(w, http.StatusBadRequest, []byte(`"Invalid data format."`))
		return
	}

	if errs := validate(u); errs != nil {
		buf := new(bytes.Buffer)
		buf.WriteString("{")
		for key, err := range errs {
			buf.WriteString(fmt.Sprintf(`"%s": "%s",`, key, err))
		}

		buf.Truncate(buf.Len() - 1)
		buf.WriteString("}")

		clientError(w, http.StatusBadRequest, buf.Bytes())
		return
	}

	u.Password, err = hashPassword(u.Password)
	if err != nil {
		serverError(w)
		fmt.Println(err)
		return
	}

	user, _ := a.UserModel.FindUserByUsername(u.Username)
	if user != nil {
		clientError(w, http.StatusBadRequest, []byte(`"Username is already in use"`))
		return
	}

	user, _ = a.UserModel.FindUserByEmail(u.Email)
	if user != nil {
		clientError(w, http.StatusBadRequest, []byte(`"Email is already in use"`))
		return
	}

	err = a.UserModel.Save(u)
	if err != nil {
		serverError(w)
		fmt.Println(err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write([]byte(`{"message": "User successfully created"}`))
}

func (a *App) login(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		serverError(w)
		fmt.Println(err)
		return
	}

	var u models.User
	err = json.Unmarshal(body, &u)
	if err != nil {
		clientError(w, http.StatusBadRequest, []byte(`"Invalid data format."`))
		return
	}

	v := validator.NewUserValidator()
	valid := v.ValidateUsername(u.Username)
	if !valid {
		clientError(w, http.StatusBadRequest, []byte(`"Username is not valid."`))
		return
	}
	valid = v.ValidatePassword(u.Password)
	if !valid {
		clientError(w, http.StatusBadRequest, []byte(`"Password is not valid."`))
		return
	}

	user, _ := a.UserModel.FindUserByUsername(u.Username)
	if user == nil {
		clientError(w, http.StatusBadRequest, []byte(`"Wrong credentials."`))
		fmt.Println(err)
		return
	}

	validPass := compareHash(user.Password, u.Password)
	if !validPass {
		clientError(w, http.StatusBadRequest, []byte(`"Wrong credentials."`))
		return
	}

	s, err := tokens.NewTokenService("/run/secrets/jwt-secret-key")
	if err != nil {
		serverError(w)
		fmt.Println(err)
		return
	}

	token, err := s.GenerateToken(user)
	if err != nil {
		serverError(w)
		fmt.Println(err)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	cookie := &http.Cookie{
		Name:     "access_token",
		Path:     "/",
		Value:    token,
		MaxAge:   86400,
		HttpOnly: true,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, cookie)

	if user.IsAdmin {
		admnCookie := &http.Cookie{
			Name:     "admin",
			Path:     "/",
			Value:    fmt.Sprint(user.IsAdmin),
			MaxAge:   0, //expires immediately
			SameSite: http.SameSiteStrictMode,
		}
		http.SetCookie(w, admnCookie)
	}

	w.WriteHeader(200)
	w.Write([]byte(`{"message": "Successfully logged in."}`))
}

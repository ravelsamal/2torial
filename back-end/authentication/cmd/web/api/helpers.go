package main

import (
	"fmt"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/models"
	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/validator"
	"golang.org/x/crypto/bcrypt"
)

func serverError(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	payload := []byte(`{"message":"Internal server error"}`)
	w.Write(payload)
}

func clientError(w http.ResponseWriter, statusCode int, message []byte) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	payload := []byte(fmt.Sprintf(`{"message":%s}`, message))
	w.Write(payload)
}

func validate(u models.User) map[string]string {
	validate := validator.NewUserValidator()

	validate.ValidateFullName(u.FullName)
	validate.ValidateUsername(u.Username)
	validate.ValidateEmail(u.Email)
	validate.ValidatePassword(u.Password)

	if len(validate.FieldErrors) != 0 {
		return validate.FieldErrors
	}

	return nil
}

func hashPassword(pass string) (string, error) {
	hashBytes, err := bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashBytes), nil
}

func compareHash(hash, plain string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(plain))

	return err == nil
}

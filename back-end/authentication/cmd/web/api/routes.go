package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (a *App) routes() http.Handler {
	r := httprouter.New()

	r.HandlerFunc(http.MethodPost, "/register", a.register)
	r.HandlerFunc(http.MethodPost, "/login", a.login)

	return r
}

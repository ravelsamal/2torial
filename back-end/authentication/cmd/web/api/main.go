package main

import (
	"log"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/db"
	"gitlab.com/ravelsamal/2torial/back-end/auth/internal/models"
)

type App struct {
	UserModel *models.UserModel
}

func main() {

	pool, err := db.OpenDB("postgresql://auth_service:auth_service@auth-db:5432/auth_service")
	if err != nil {
		log.Fatalln(err)
	}
	defer pool.Close()

	userModel := models.NewUserModel(pool)
	app := App{
		UserModel: userModel,
	}

	s := http.Server{
		Addr:    ":80",
		Handler: app.routes(),
	}

	s.ListenAndServe()
}

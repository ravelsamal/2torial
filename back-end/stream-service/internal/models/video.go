package models

import (
	"context"

	"github.com/jackc/pgx/v5/pgxpool"
)

type Video struct {
	ID          string
	Title       string
	Description string
	URL         string
	Author      string
	Thumbnail   string
	Published   bool
}

type VideoModel struct {
	db *pgxpool.Pool
}

func NewVideoModel(db *pgxpool.Pool) *VideoModel {
	return &VideoModel{
		db: db,
	}
}

func (vm VideoModel) Save(v Video) error {
	stmt := `INSERT INTO video(id, title, description, url, author, thumbnail, published)
		VALUES ($1, $2, $3, $4, $5, $6, $7)`
	_, err := vm.db.Exec(context.Background(), stmt, v.ID, v.Title, v.Description, v.URL, v.Author, v.Thumbnail, false)
	if err != nil {
		return err
	}

	return nil
}

func (vm VideoModel) IDExists(id string) bool {
	stmt := "SELECT id FROM video WHERE id = $1;"
	row := vm.db.QueryRow(context.Background(), stmt, id)

	err := row.Scan()
	if err == nil {
		return true
	}

	return false
}

package storage

import (
	"context"
	"io"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type FileSaver interface {
	Save(r io.Reader, key string) error
}

type Storage struct {
	client *s3.Client
}

func NewS3() (*Storage, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithRegion("us-east-1"))
	if err != nil {
		return nil, err
	}
	return &Storage{
		client: s3.NewFromConfig(cfg),
	}, nil
}

func (s *Storage) Save(f io.Reader, key string) error {
	_, err := s.client.PutObject(context.Background(), &s3.PutObjectInput{
		Bucket: aws.String("2torial"),
		Key:    aws.String(key),
		Body:   f,
	})
	if err != nil {
		return err
	}

	return nil
}

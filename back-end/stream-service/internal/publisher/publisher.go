package publisher

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
)

type Publisher interface {
	Publish(string) error
}

type SQS struct {
	client *sqs.Client
}

func NewSQS() (*SQS, error) {
	cfg, err := config.LoadDefaultConfig(context.TODO())
	if err != nil {
		return nil, err
	}
	return &SQS{
		client: sqs.NewFromConfig(cfg),
	}, nil
}

func (pub *SQS) Publish(msg string) error {
	queueURL, err := pub.client.GetQueueUrl(context.Background(), &sqs.GetQueueUrlInput{
		QueueName: aws.String("videosToBeProcessed"),
	})
	if err != nil {
		return err
	}
	_, err = pub.client.SendMessage(context.Background(), &sqs.SendMessageInput{
		MessageBody: aws.String(msg),
		QueueUrl:    queueURL.QueueUrl,
	})
	if err != nil {
		return err
	}
	return nil
}

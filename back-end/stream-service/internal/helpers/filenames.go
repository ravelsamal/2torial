package helpers

import (
	"crypto/rand"
	"encoding/base64"
	"strings"
)

// Takes a string and split it by "." then returns the last part of it.
// Returns ErrNoExtension if there only one part.
func FileExtension(filename string) (string, error) {
	parts := strings.Split(filename, ".")
	if len(parts) == 1 {
		return "", ErrNoExtension
	}

	return parts[len(parts)-1], nil
}

func GenerateID(size int) (string, error) {
	b := make([]byte, size)

	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	id := base64.URLEncoding.EncodeToString(b)

	return id, nil
}

func AllowedVideoFormat(format string) bool {
	var allowedVideoFormats = []string{
		"video/x-msvideo",
		"video/mp4",
		"video/mpeg",
		"video/webm",
	}

	for _, v := range allowedVideoFormats {
		if format == v {
			return true
		}
	}

	return false
}

func AllowedThumbnailFormat(format string) bool {
	var allowedThumbnailFormats = []string{
		"image/jpeg",
		"image/png",
		"image/svg+xml",
	}

	for _, v := range allowedThumbnailFormats {
		if format == v {
			return true
		}
	}

	return false
}

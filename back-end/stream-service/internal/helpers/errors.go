package helpers

import "errors"

var ErrNoExtension = errors.New("The file has no extension.")

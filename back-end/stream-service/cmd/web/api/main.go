package main

import (
	"log"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/db"
	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/models"
	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/publisher"
	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/storage"
)

type App struct {
	model     *models.VideoModel
	store     *storage.Storage
	publisher publisher.Publisher
	helper    *helper
}

func main() {
	pool, err := db.OpenDB("postgresql://auth_service:auth_service@auth-db:5432/auth_service")
	if err != nil {
		log.Fatalln(err)
	}
	defer pool.Close()

	store, err := storage.NewS3()
	if err != nil {
		log.Println(err)
	}

	sqsClient, err := publisher.NewSQS()
	if err != nil {
		log.Println(err)
	}

	model := models.NewVideoModel(pool)
	helper := newHelper(model)
	app := &App{
		helper:    helper,
		model:     model,
		publisher: sqsClient,
		store:     store,
	}

	r := app.routes()
	s := http.Server{
		Addr:    ":80",
		Handler: r,
	}
	s.ListenAndServe()
}

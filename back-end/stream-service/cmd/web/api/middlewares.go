package main

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/helpers"
)

func addHeaders(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Access-Control-Allow-Origin", "http://localhost")

		if r.Method == "OPTIONS" {
			http.Error(w, "No Content", http.StatusNoContent)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func auth(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("access_token")
		if err != nil {
			clientError(w, http.StatusUnauthorized, "Not authorized")
			fmt.Println(err)
			return
		}
		ts, err := helpers.NewTokenService("/run/secrets/jwt-secret-key")
		if err != nil {
			serverError(w)
			fmt.Println(err)
			return
		}

		claims, err := ts.VerifyToken(cookie.Value)
		if err != nil {
			serverError(w)
			fmt.Println(err)
			return
		}

		ctx := context.WithValue(r.Context(), "username", claims.Username)
		r = r.WithContext(ctx)

		next.ServeHTTP(w, r)
	})
}

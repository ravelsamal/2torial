package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (h *App) routes() http.Handler {
	r := httprouter.New()
	r.HandlerFunc(http.MethodPost, "/videos/upload", auth(h.upload))
	r.ServeFiles("/videos/*filepath", http.Dir("/videos/"))

	return addHeaders(r)
}

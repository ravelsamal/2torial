package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/helpers"
	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/models"
)

func (app *App) upload(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(0)
	if err != nil {
		clientError(w, http.StatusBadRequest, "Invalid data provided")
		log.Println(err)
		return
	}

	video, videoHeaders, err := r.FormFile("video")
	if err != nil {
		clientError(w, http.StatusBadRequest, "Invalid data provided")
		log.Println(err)
		return
	}

	typ := videoHeaders.Header.Get("Content-Type")
	if ok := helpers.AllowedVideoFormat(typ); !ok {
		clientError(w, http.StatusBadRequest, "Video format is not supported")
		log.Println(err)
		return
	}

	videoExtension, err := helpers.FileExtension(videoHeaders.Filename)
	if err != nil {
		clientError(w, http.StatusBadRequest, "Video format is not supported")
		return
	}

	thumbnail, thumbnailHeaders, err := r.FormFile("thumbnail")
	if err != nil {
		clientError(w, http.StatusBadRequest, "Invalid data provided")
		log.Println(err)
		return
	}

	typ = thumbnailHeaders.Header.Get("Content-Type")
	if ok := helpers.AllowedThumbnailFormat(typ); !ok {
		clientError(w, http.StatusBadRequest, "Thumbnail format is not supported")
		log.Println(err)
		return
	}

	thumbnailExtension, err := helpers.FileExtension(thumbnailHeaders.Filename)
	if err != nil {
		clientError(w, http.StatusBadRequest, "Thumbnail format is not supported")
		return
	}

	id, err := app.helper.uniqIDGen()
	if err != nil {
		serverError(w)
		log.Println(err)
		return
	}

	err = app.store.Save(video, fmt.Sprintf("videos/%s/video.%s", id, videoExtension))
	if err != nil {
		serverError(w)
		log.Println(err)
		return
	}

	err = app.store.Save(thumbnail, fmt.Sprintf("videos/%s/thumbnail.%s", id, thumbnailExtension))
	if err != nil {
		serverError(w)
		log.Println(err)
		return
	}

	username := r.Context().Value("username").(string)
	vid := models.Video{
		ID:          id,
		Title:       r.PostFormValue("title"),
		Description: r.PostFormValue("description"),
		Author:      username,
		URL:         fmt.Sprintf("/stream/videos/%s/video.m3u8", id),
		Thumbnail:   fmt.Sprintf("/stream/videos/%s/thumbnail.%s", id, thumbnailExtension),
	}

	err = app.model.Save(vid)
	if err != nil {
		serverError(w)
		log.Println(err)
		return
	}

	message, err := json.Marshal(vid)
	if err != nil {
		log.Println(err)
		serverError(w)
		return
	}

	err = app.publisher.Publish(string(message))
	if err != nil {
		serverError(w)
		log.Println(err)
		return
	}

	sendJSON(w, http.StatusCreated, "Video was successfully uploaded")
}

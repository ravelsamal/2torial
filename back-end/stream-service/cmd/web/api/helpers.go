package main

import (
	"encoding/json"
	"net/http"

	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/helpers"
	"gitlab.com/ravelsamal/2torial/back-end/stream-service/internal/models"
)

type helper struct {
	m *models.VideoModel
}

func newHelper(m *models.VideoModel) *helper {
	return &helper{
		m,
	}
}

func serverError(w http.ResponseWriter) {
	sendJSON(w, http.StatusInternalServerError, "Internal server error")
}

func clientError(w http.ResponseWriter, statusCode int, message any) {
	sendJSON(w, statusCode, message)
}

func sendJSON(w http.ResponseWriter, statusCode int, message any) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	payload := map[string]any{"messgae": message}
	byteData, _ := json.Marshal(payload)
	w.Write(byteData)
}

func (h *helper) uniqIDGen() (string, error) {
	var err error
	var id string

	for {
		id, err = helpers.GenerateID(8)
		if err != nil {
			return "", err
		}

		exists := h.m.IDExists(id)
		if !exists {
			break
		}
	}

	return id, nil
}
